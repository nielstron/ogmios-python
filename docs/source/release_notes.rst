Release Notes
=============

Version 1.1.1
-------------

* Add new queries to documentation

Version 1.1.0
-------------

* Added support for Ogmios server v6.5.0 release
* Added new query_constitution, query_constitutional_committee, and query_treasury_and_reserves methods
* Added additional_headers parameter to Client class to allow for custom headers to be sent with requests (thanks @linconvidal!)

Version 1.0.6
-------------

* PyCardano compatibility improvements

Version 1.0.5
-------------

* Removed dependency version restriction for `pycardano` and upgrade `pydantic` version to >= 2.0 for better compatibility
* Remove Ogmios client from chain_context context manager to improve compatibility with PyCardano
* Fix issue preventing logs from being printed to the console when importing `ogmios` module

Version 1.0.1
-------------

* Fixed a schema error for Byron era blocks when using the chainsync mini-protocol
* Improved OgmiosChainContext module for working with PyCardano

Version 1.0.0
-------------

* Support Ogmios server v6.0.0 release (handle changes in responses containing ADA values)
* Throw an exception if Ogmios server < v6.0.0 is detected
* Added utility functions (and new example) for getting mempool contents, and converting TX dicts to PyCardano objects


Version 0.4.0
-------------

* Added full client implementation of the Ogmios transaction submission mini-protocol.


Version 0.3.0
-------------

* Added full client implementation of the Ogmios mempool mini-protocol.


Version 0.2.1
-------------

* Added full client implementation of the Ogmios ledger state queries mini-protocol.


Version 0.1.4
-------------

* Added full client implementation of the Ogmios chain synchonrization mini-protocol.