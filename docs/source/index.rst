.. ogmios-python documentation master file, created by
   sphinx-quickstart on Fri Oct 20 17:15:18 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ogmios-python documentation
============================

`Ogmios <https://ogmios.dev/>`_ is a lightweight bridge interface for cardano-node. It offers a WebSockets API that enables local clients to speak Ouroboros' mini-protocols via JSON/RPC. 
**ogmios-python** is an Ogmios client written in Python designed for ease of use.

This client library currently only supports Ogmios v6.0.0 and above.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started
   examples/index
   api/index
   release_notes